--Dado un número devuelve su sucesor
sucesor :: Int -> Int
sucesor a = a+1

--Dados dos números devuelve su suma utilizando la operación 
sumar :: Int -> Int -> Int
sumar a b = a+b

-- dos números devuelve el mayor de estos
maximo :: Int -> Int -> Int
maximo a b = 
	if a>b then a
	else b


{----------------------------------------------------------------------------------------------------------
                      Defina las siguientes funciones utilizando pattern matching
----------------------------------------------------------------------------------------------------------}

{-Dado un booleano, si es T rue devuelve F alse, y si es F alse devuelve T rue.
Definida en Haskell como not.-}
negar :: Bool -> Bool
negar a = not a

{-Dados dos booleanos si ambos son T rue devuelve T rue, sino devuelve F alse.
Definida en Haskell como &&.-}
andLogico :: Bool -> Bool -> Bool
andLogico True True = True
andLogico a b = False 

{-Dados dos booleanos si alguno de ellos es T rue devuelve T rue, sino devuelve F alse.
Definida en Haskell como ||.-}
orLogico :: Bool -> Bool -> Bool	
orLogico True False = True
orLogico False True = True
orLogico True True = True
orLogico a b = False

t = True
f = False

{-Dado un par de números devuelve la primera componente.
Definida en Haskell como fst.-}
primera :: (Int,Int) -> Int
primera (a,b) = a

{-Dado un par de números devuelve la segunda componente.
Definida en Haskell como snd.-}
segunda :: (Int,Int) -> Int
segunda (a,b) = b

{-Dado un par de números devuelve su suma.-}
sumaPar :: (Int,Int) -> Int
sumaPar (a,b) = a+b

{-Dado un par de números devuelve el mayor de estos.-}
maxDelPar :: (Int,Int) -> Int
maxDelPar (a,b) = maximo a b

--Dado un elemento de algún tipo devuelve ese mismo elemento.
loMismo :: a -> a
loMismo a = a 

--Dado un elemento de algún tipo devuelve el número 7.
siempreSiete :: a -> Int
siempreSiete a = 7 

--Dado un elemento de algún tipo devuelve un par con ese elemento en ambas componentes.
duplicar :: a -> (a,a)
duplicar a = (a,a)

--Dado un elemento de algún tipo devuelve una lista con este único elemento.
singleton :: a -> [a]
singleton a = [a] 


{-----------------------------------------------------------------------------------------------------
       Defina las siguientes funciones polimórficas utilizando pattern matching sobre listas
-----------------------------------------------------------------------------------------------------}

--Dada una lista de elementos, si es vacía devuelve True, sino devuelve False.
isEmpty :: [a] -> Bool
isEmpty [] = True
isEmpty [a] = False

--Dada una lista devuelve su primer elemento.
head' :: [a] -> a
--head' []  null---------------------------------------------------------------------------------------
head' (primerElemento:elResto) = primerElemento

--Dada una lista devuelve esa lista menos el primer elemento.
tail' :: [a] -> [a]
tail' (primerElemento:elResto) = elResto 


{------------------------------------------------------------------------------------------------
Defina las siguientes funciones utilizando recursión estructural sobre listas, salvo que se indique 
lo contrario
----------------------------------------------------------------------------------------------------}

--Dada una lista de enteros devuelve la suma de todos sus elementos.
sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

{-Dada una lista de elementos de algún tipo devuelve el largo de esa lista, es decir, la cantidad 
de elementos que posee.-}
longitud :: [a] -> Int
longitud [] = 0
longitud (x:xs) = 1+longitud xs

--Dada una lista de enteros, devuelve la lista de los sucesores de cada entero.
mapSucesor :: [Int] -> [Int]
mapSucesor [] = []
mapSucesor (x:xs) = [x+1] ++ mapSucesor xs

{-Dada una lista de pares de enteros, devuelve una nueva lista en la que cada elemento es la
suma de los elementos de cada par.-}
mapSumaPar :: [(Int,Int)] -> [Int]
mapSumaPar [] = []
mapSumaPar (x:xs) = [sumaPar x] ++ mapSumaPar xs

{-Dada una lista de pares, devuelve una nueva lista en la que cada elemento es el mayor de
las componentes de cada par.-}
mapMaxDelPar :: [(Int,Int)] -> [Int]
mapMaxDelPar [] = []
mapMaxDelPar (x:xs) = [maxDelPar x] ++ mapMaxDelPar xs

--Dada una lista de booleanos devuelve True si todos sus elementos son True.
todoVerdad :: [Bool] -> Bool
todoVerdad [] = True 
--duda si lleva false la func. va a dar siempre false pero si lleva true va a dar verdadero cuando 
--este vacia, tendria que ver si tiene algo darle el valor true pero si esta vacia darle el valor false
todoVerdad (x:xs) = andLogico x (todoVerdad xs)

--Dada una lista de booleanos devuelve True si alguno de sus elementos es True.
algunaVerdad :: [Bool] -> Bool
algunaVerdad [] = False
algunaVerdad (x:xs) = orLogico x (algunaVerdad xs)

--Dados un elemento e y una lista xs devuelve True si existe un elemento en xs que sea igual a e.
pertenece :: Eq a => a -> [a] -> Bool
pertenece a [] = False
pertenece a (x:xs) = 
	let booleano = a==x
	in if (booleano)
			then booleano
			else pertenece a xs
 
--Dados un elemento e y una lista xs cuenta la cantidad de apariciones de e en xs.
apariciones :: Eq a => a -> [a] -> Int
apariciones a [] = 0
apariciones a (x:xs) =
	if a==x
		then 1 + apariciones a xs
		else 0 + apariciones a xs 

--Dados un número n y una lista xs, devuelve todos los elementos de xs que son menores a n.
filtrarMenoresA :: Int -> [Int] -> [Int]
filtrarMenoresA a [] = []
filtrarMenoresA a (x:xs)=
	if a>x
		then x:filtrarMenoresA a xs
		else filtrarMenoresA a xs

--Dados un elemento y una lista filtra (elimina) todas las ocurrencias de ese elemento en la lista.
filtrarElemento :: Eq a => a -> [a] -> [a]
filtrarElemento a [] = []
filtrarElemento a (x:xs) = 
	if a/=x
		then x : filtrarElemento a xs
		else filtrarElemento a xs

{-Dada una lista de listas, devuelve la lista de sus longitudes. 
Aplique esta función a la lista
de strings ["Estructuras", "de", "datos"] y observe el resultado.-}
mapLongitudes :: [[a]] -> [Int]
mapLongitudes [] = []
mapLongitudes (x:xs) = length x : mapLongitudes xs

--Dados un número n y una lista de listas, devuelve la lista de aquellas listas que tienen más de n elementos.
longitudMayorA :: Int -> [[a]] -> [[a]]
longitudMayorA n [] = []
longitudMayorA n (x:xs) = 
	if length x < n
		then x : longitudMayorA n xs
		else longitudMayorA n xs

{-Dado un elemento e y una lista xs, ubica a e entre medio de todos los elementos de xs.
Ejemplo: intercalar ’,’ "abcde" == "a,b,c,d,e"-}
intercalar :: a -> [a] -> [a]
intercalar a [] = []
intercalar a (x:xs) = x : a : intercalar a xs

--Dados una lista y un elemento, devuelve una lista con ese elemento agregado al final de la lista.
--snoc :: [a] -> a -> [a]

{-Dadas dos listas devuelve la lista con todos los elementos de la primera lista y todos los
elementos de la segunda a continuación. Definida en Haskell como ++.-}
--append :: [a] -> [a] -> [a]

--Dada una lista de listas, devuelve una única lista con todos sus elementos.
--aplanar :: [[a]] -> [a]

{-Dada una lista devuelve la lista con los mismos elementos de atrás para adelante. Definida 
en Haskell como reverse.-}
--reversa :: [a] -> [a]

{-Dadas dos listas de enteros, devuelve una lista donde el elemento en la posición n es el
máximo entre el elemento n de la primera lista y de la segunda lista, teniendo en cuenta que
las listas no necesariamente tienen la misma longitud.-}
--zipMaximos :: [Int] -> [Int] -> [Int]

{-Dadas dos listas de enteros de igual longitud, devuelve una lista de pares (min, max), donde
min y max son el mínimo y el máximo entre los elementos de ambas listas en la misma posición.-}
--zipSort :: [Int] -> [Int] -> [(Int, Int)]

{-Dada una lista de enteros, devuelve un número que es el promedio entre todos los elementos
de la lista. ¿Pudo resolverla utilizando recursión estructural?-}
--promedio :: [Int] -> Int

--Dada una lista devuelve el mínimo
--minimum :: Ord a => [a] -> a



{-Defina las siguientes funciones utilizando recursión sobre números enteros, 
salvo que se indique lo contrario:-}

{-Dado un número n se devuelve la multiplicación de este número y todos sus
 anteriores hasta llegar a 0. Si n es 0 devuelve 1. 
 La función es parcial si n es negativo.-}
factorial :: Int -> Int
factorial 0 = 1
factorial n =
	n + factorial (n-1)

{-Dado un número n devuelve una lista cuyos elementos sean los números comprendidos 
ntre n y 1 (incluidos). Si el número es inferior a 1, devuelve la lista vacía.-}
cuentaRegresiva :: Int -> [Int]
cuentaRegresiva 0 = []
cuentaRegresiva n = n : cuentaRegresiva (n-1)

{-Dado un número n devuelve una lista cuyos elementos sean los números entre 1 y n 
(incluidos).
contarHasta :: Int -> [Int]
contarHasta 0 = []
contarHasta n = 
	if (last (contarHasta n)) /= n 
	contarHasta n : contarHasta ((last (contarHasta n)) +1)
	--	else contarHasta n -}

{-Dado un número n y un elemento e devuelve una lista en la que el elemento e 
repite n veces.-}
replicarN :: Int -> a -> [a]
replicarN 0 a = []
replicarN n a =	a : replicarN (n-1) a
	
{-Dados dos números n y m devuelve una lista cuyos elementos sean los números
 entre n y m (incluidos).-}
desdeHasta :: Int -> Int -> [Int]
desdeHasta 0 0 = []
desdeHasta n m = 
	if n /= m
	then n : desdeHasta (n+1) m 
	else desdeHasta 0 0 ++ [m]

{-Dados un número n y una lista xs, devuelve una lista con los primeros n elementos 
de xs. Si la lista posee menos de n elementos, se devuelve una lista vacía.
takeN :: Int -> [a] -> [a]
takeN 0 [a] = []
takeN n [a] = -}

{-Dados un número n y una lista xs, devuelve una lista sin los primeros n elementos 
de lista recibida. Si xs posee menos de n elementos, se devuelve la lista completa.-}
--dropN :: Int -> [a] -> [a]

{-Dados un número n y una lista xs, devuelve un par donde la primera componente es
 la lista que resulta de aplicar takeN a xs, y la segunda componente el resultado de
 aplicar dropN a xs. ¿Conviene utilizar recursión?-}
--splitN :: Int -> [a] -> ([a], [a])


-----------------------------------------PRACTICA 2-----------------------------------------------------------

{-1. Definir el tipo de dato Dir, con las alternativas Norte, Sur, Este y Oeste.
 Luego implementar las siguientes funciones:--}

{-Dada una dirección devuelve su opuesta
opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Sur = Norte
opuesto Este = Sur
opuesto Oeste = Sur-}

{-Dada una dirección devuelve su siguiente, en sentido horario.-}
--siguiente :: Dir -> Dir


{-2. Definir el tipo de dato Persona, como un nombre la edad de la persona. 
Realizar las siguientes funciones:-}

--Devuelve el nombre de una persona
--nombre :: Persona -> String

--Devuelve la edad de una persona
--edad :: Persona -> Int

--Dada una persona la devuelve con su edad aumentada en 1.
--crecer :: Persona -> Persona

--Dados un nombre y una persona, reemplaza el nombre de la persona por este otro.
--cambioDeNombre :: String -> Persona -> Persona

--Dadas dos personas indica si la primera es más joven que la segunda.
--esMenorQueLaOtra :: Persona -> Persona -> Bool

{-Dados una edad y una lista de personas devuelve todas las personas que son mayores
a esa edad.-}
--mayoresA :: Int -> [Persona] -> [Persona]

{-Dada una lista de personas devuelve el promedio de edad entre esas personas. 
La lista al menos posee una persona.-}
--promedioEdad :: [Persona] -> Int

{-Dada una lista de personas devuelve la persona más vieja de la lista. La lista al menos
posee una persona.-}
--elMasViejo :: [Persona] -> Persona





{-

{-3. Definir los tipos de datos Pokemon, como un TipoDePokemon 
(agua, fuego o planta) y un porcentaje de energía; y Entrenador, como un nombre y una lista de Pokemon. 
Luego definir las siguientes funciones:-}

data TipoDePokemon = Agua |Fuego |Planta deriving (Show, Eq)
data Pokemon = ElPokemon TipoDePokemon Int deriving (Show)
data Entrenador = ElEntrenador Nombre [Pokemon]  deriving (Show)
type Nombre = String  

squirtle = ElPokemon Agua 20
bulbasour = ElPokemon Planta 35
charizard = ElPokemon Fuego 90
ash = ElEntrenador "Ash" [squirtle, bulbasour, charizard]

{-Dados dos pokemon indica si el primero indudablemente le gana al segundo. Agua gana
a fuego, fuego a planta y planta a agua. En cualquier otro caso no podemos afirmarlo.-}
--funcion auxiliar
tipoDePokemon :: Pokemon -> TipoDePokemon
tipoDePokemon (ElPokemon tipo energía) = tipo

--leGanaAlToque :: Pokemon -> Pokemon -> Bool
--leGanaAlToque (ElPokemon p1) (ElPokemon p2) = 

--Agrega un pokemon a la lista de pokemon del entrenador.
capturarPokemon :: Pokemon -> Entrenador -> Entrenador
capturarPokemon p (ElEntrenador nombre pokemones) =
	ElEntrenador nombre (p : pokemones)

--Devuelve la cantidad de pokemons que posee el entrenador.
cantidadDePokemons :: Entrenador -> Int
cantidadDePokemons (ElEntrenador nombre pokemones) = longitud pokemones

--Devuelve la cantidad de pokemons de determinado tipo que posee el entrenador.
cantidadDePokemonsDeTipo :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonsDeTipo tipo (ElEntrenador nombre (x:xs)) =
	if tipo == tipoDePokemon x --no usar Eq en los tipos, definir una funcion para la comparacion
		then 1 + cantidadDePokemonsDeTipo tipo (ElEntrenador nombre (x:xs))
		else cantidadDePokemonsDeTipo tipo (ElEntrenador nombre (x:xs))

--Dado un entrenador devuelve True si ese entrenador posee al menos un pokemon de
--cada tipo posible.
--esExperto :: Entrenador -> Bool


--4. Tenemos los siguientes tipos de datos

data Pizza = Prepizza 
		| Agregar Ingrediente Pizza deriving (Show)

data Ingrediente =
		Queso
		|Salsa
		|Jamon
		|AceitunasVerdes Int deriving (Show)

--Dada una pizza devuelve la lista de ingredientes
ingredientes :: Pizza -> [Ingrediente]
ingredientes Prepizza = []
ingredientes (Agregar ingrediente pizza) = ingrediente : (ingredientes pizza)

--Dice si una pizza tiene jamón
tieneJamon :: Pizza -> Bool
tieneJamon Prepizza = False
tieneJamon (Agregar ingrediente pizza) = 
	(esJamon ingrediente) || (tieneJamon pizza)

esJamon :: Ingrediente -> Bool
esJamon Jamon  = True
esJamon _ = False

pizzaConJamon = Agregar (Queso) 
					(Agregar(Jamon)
						(Agregar(AceitunasVerdes 7
							(Agregar(Salsa
				 				Prepizza)))))

--Le saca los ingredientes que sean jamón a la pizza
sacarJamon :: Pizza -> Pizza
sacarJamon (Agregar ingrediente pizza) =
	if noEsJamon ingrediente
		then (Agregar ingrediente pizza)
		else sacarJamon pizza

noEsJamon :: Ingrediente -> Bool
noEsJamon Jamon = False
noEsJamon _ = True

--Dada una lista de ingredientes construye una pizza
armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = Prepizza
armarPizza (x:xs) = Agregar x (armarPizza xs)

--Recorre cada ingrediente y si es aceitunas duplica su cantidad
--duplicarAceitunas :: Pizza -> Pizza
--duplicarAceitunas Prepizza = 

--Saca los ingredientes de la pizza que se encuentren en la lista
--sacar :: [Ingrediente] -> Pizza -> Pizza

{-Dada una lista de pizzas devuelve un par donde la primera componente es la cantidad
de jamón de la pizza que es la segunda componente.-}
--cantJamon :: [Pizza] -> [(Int, Pizza)]

--Devuelve las pizzas con más de “n” aceitunas.
--mayorNAceitunas :: Int -> [Pizza] -> [Pizza]

-}


----------------------5. Tenemos los siguientes tipos de datos----------------------------

data Objeto = Cacharro | Tesoro deriving (Show)
data Camino =  Fin | Cofre [Objeto] Camino | Nada Camino  deriving (Show)

--Definir las siguientes funciones:

cofreSinTesoro = [Cacharro, Cacharro, Cacharro] 
cofreMixto = [Cacharro, Tesoro] 
cofreConTesoro = [Tesoro, Tesoro]

caminoConTesoro = Nada (
					Nada(
						Cofre cofreMixto (
							Nada (
								Cofre cofreConTesoro (
								Fin)))))
caminoSinTesoro = Nada (
					Nada(
						Cofre cofreSinTesoro (
							Nada (
								Cofre cofreSinTesoro (
									Fin)))))

--fuc. aux.
--hayTesoroAca :: Camino -> Bool
--hayTesoroAca (Cofre xs camino) = hayTesoroEnCofre xs
--hayTesoroAca _ = False

--fuc. aux.
hayTesoroEnCofre :: [Objeto] -> Bool --tested
hayTesoroEnCofre [] = False
hayTesoroEnCofre (x:xs) = (esTesoro x) || (hayTesoroEnCofre xs)

--fuc. aux.
esTesoro :: Objeto -> Bool --tested
esTesoro Tesoro = True
esTesoro Cacharro = False

--Indica si hay un cofre con un tesoro en el camino.
hayTesoro :: Camino -> Bool
hayTesoro Fin = False
hayTesoro (Nada camino) = hayTesoro camino
hayTesoro (Cofre xs camino) = hayTesoroEnCofre xs || hayTesoro camino


{-Indica la cantidad de pasos que hay que recorrer hasta llegar al primer cofre con un
tesoro. Si un cofre con un tesoro está al principio del camino, la cantidad de pasos a
recorrer es 0.-}
--precondicion: hay un tesoro en el camino
pasosHastaTesoro :: Camino -> Int
pasosHastaTesoro (Nada camino) = 1 + (pasosHastaTesoro camino)
pasosHastaTesoro (Cofre objetos camino) = 
	if hayTesoroEnCofre objetos 
		then 0
		else 1 + pasosHastaTesoro camino

{-Indica si hay un tesoro en una cierta cantidad exacta de pasos. Por ejemplo, si el número
de pasos es 5, indica si hay un tesoro en 5 pasos.-}
hayTesoroEn :: Int -> Camino -> Bool 	--tested
hayTesoroEn n Fin = False
hayTesoroEn n (Nada camino) =  hayTesoroEn (n-1) camino
hayTesoroEn n (Cofre obj camino) = 
	if (n>0) 
		then hayTesoroEn (n-1) camino
		else hayTesoroEnCofre obj


--Indica si hay al menos “n” tesoros en el camino.
alMenosNTesoros :: Int -> Camino -> Bool
alMenosNTesoros n Fin = n==0
alMenosNTesoros n (Nada camino) = alMenosNTesoros n camino
alMenosNTesoros n (Cofre objetos camino) = 
	if  n>0 
		then alMenosNTesoros (n - (cantidadDeTesorosEnCofre objetos)) camino
		else alMenosNTesoros n camino

--func. aux.	tested
cantidadDeTesorosEnElCamino :: Camino -> Int
cantidadDeTesorosEnElCamino Fin = 0
cantidadDeTesorosEnElCamino (Nada camino) = cantidadDeTesorosEnElCamino camino
cantidadDeTesorosEnElCamino (Cofre objetos camino) = 
	if hayTesoroEnCofre objetos
		then (cantidadDeTesorosEnCofre objetos) + cantidadDeTesorosEnElCamino camino
		else cantidadDeTesorosEnElCamino camino

--func. aux.	tested
cantidadDeTesorosEnCofre :: [Objeto] -> Int
cantidadDeTesorosEnCofre [] = 0
cantidadDeTesorosEnCofre (x:xs) = 
	if esTesoro x 
		then 1 + cantidadDeTesorosEnCofre xs
		else cantidadDeTesorosEnCofre xs

{-Dado un rango de pasos, indica la cantidad de tesoros que hay en ese rango. Por ejemplo,
si el rango es 3 y 5, indica la cantidad de tesoros que hay entre hacer 3 pasos y hacer 5. 
Están incluidos tanto 3 como 5 en el resultado.-}
--cantTesorosEntre :: Int -> Int -> Camino -> Int







