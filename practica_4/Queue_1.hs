{-
1. Implemente el tipo abstracto Queue utilizando listas. Los elementos deben encolarse por el
final de la lista y desencolarse por delante.
-}

module Queue_1 where

data Queue a = Q [a] deriving(Show)

ls1 :: Queue Int
ls1 = Q [1,2,3]

ls2 :: Queue a
ls2 = Q []

--Crea una cola vacía.
emptyQ :: Queue a
emptyQ = Q []

--Dada una cola indica si la cola está vacía.
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q xs) = length(xs) == 0

--Dados un elemento y una cola, agrega ese elemento a la cola.
queue :: a -> Queue a -> Queue a
queue a (Q xs) = Q (xs ++ [a])

--Dada una cola devuelve el primer elemento de la cola.
firstQ :: Queue a -> a
firstQ (Q xs) = head xs

--Dada una cola la devuelve sin su primer elemento.
dequeue :: Queue a -> Queue a
dequeue (Q []) = Q []
dequeue (Q xs) = Q (tail xs)
