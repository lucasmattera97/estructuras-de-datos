module Stack where

data Stack a = S [a] deriving (Show)

st1 :: Stack Int
st1 = S []

st2 :: Stack Int
st2 = S [1,2,3] 

{-
Una Stack es un tipo abstracto de datos de naturaleza LIFO (last in, first out). 
Esto significa
que los últimos elementos agregados a la estructura son los primeros en salir
 (como en una pila de
platos). Su interfaz es la siguiente:
-}

--Crea una pila vacía.
emptyS :: Stack a
emptyS = S []

--Dada una pila indica si está vacía.
isEmptyS :: Stack a -> Bool
isEmptyS (S []) = True
isEmptyS (S _) = False

--Dados un elemento y una pila, agrega el elemento a la pila.
push :: a -> Stack a -> Stack a
push e (S xs) = S (e:xs)

--Dada un pila devuelve el elemento del tope de la pila.
top :: Stack a -> a
top (S xs) = last xs

--Dada una pila devuelve la pila sin el primer elemento.
pop :: Stack a -> Stack a
pop (S xs) = S (tail xs)