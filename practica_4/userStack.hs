import Stack

{-
Crea una pila vacía.
emptyS :: Stack a

Dada una pila indica si está vacía.
isEmptyS :: Stack a -> Bool

Dados un elemento y una pila, agrega el elemento a la pila.
push :: a -> Stack a -> Stack a

Dada un pila devuelve el elemento del tope de la pila.
top :: Stack a -> a

Dada una pila devuelve la pila sin el primer elemento.
pop :: Stack a -> Stack a
-}

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)

st = S [1,2,3,4,5,6]

{-
Como usuario del tipo abstracto Stack implementar las siguientes funciones:
-}

--Dada una lista devuelve una pila sin alterar el orden de los elementos.
apilar :: [a] -> Stack a
apilar [] = emptyS
apilar (x:xs) =  push x (apilar xs) 

{-
--Dada una pila devuelve una lista sin alterar el orden de los elementos.
desapilar :: Stack a -> [a]
desapilar st = 
	if isEmptyS st
		then []
		else (top st) : (desapilar (pop st))
el top me devuelve el ultimo elemento y el pop el primero, no se como pasar a 
desapilar el arbol sin el ultimo elemento
-}

--Dado un árbol devuelve una pila con los elementos apilados inorder.
treeToStack :: Tree a -> Stack a
treeToStack EmptyT = emptyS
--treeToStack (x:xs) [] = push x (treeToStack xs)
--treeToStack [] (y:ys)  = push y (treeToStack ys)

treeToStack (x:xs) (y:ys) = push (push x (treeToStack xs)) (push y (treeToStack ys))
--no puedo usar el push para concatenar la rama izquierda ocn la derecha porque el push recibe un elemento, no un stack 

{-
{-
Toma un string que representa una expresión aritmética, por ejemplo ”(2 + 3) ∗ 2”,
y verifica que la cantidad de paréntesis que abren se corresponda con los que cierran.
Para hacer esto utilice una stack. Cada vez que encuentra un paréntesis que abre, lo
apila. Si encuentra un paréntesis que cierra desapila un elemento. Si al terminar de
recorrer el string se desapilaron tantos elementos como los que se apilaron, ni más ni
menos, entonces los paréntesis están balaceados. Pista: recorra una stack pasada por
parámetro a una subtarea que devuelve un booleano, que indifica si los parentesis están
balanceados.
balanceado :: String -> Bool (desafío, opcional)
-}
-}