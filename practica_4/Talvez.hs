module Talvez where

data Talvez a = Nada | Solo a deriving(Show)

--1. Defina las siguientes operaciones (notar que son parciales si no devuelven Maybe):
--2. Indicar los ordenes de complejidad en peor caso de cada función implementada.

--Versión de head que es total
headM :: [a] -> Talvez a
headM [] = Nada
headM (x:xs) = Solo x

--Dada una lista, devuelve su último elemento.
lastM :: [a] -> Talvez a
lastM [] = Nada
lastM xs = Solo (last xs)

{-	
--Dada una lista de elementos devuelve el máximo.
maximumM :: Ord a => [a] -> Talvez a
maximumM [] = Nada
maximumM (x:xs) = 

--Dada una lista quita su último elemento.
initM :: [a] -> Talvez [a]

--Dado un índice devuelve el elemento de esa posición.
get :: Int -> [a] -> Talvez a

{-
Dado un elemento y una lista devuelve la posición de la lista en la que se encuentra
dicho elemento.
-}
indiceDe :: Eq a => a -> [a] -> Talvez Int

--Dada una lista de pares (clave, valor) y una clave devuelve el valor asociado a la clave.
lookupM :: Eq k => [(k,v)] -> k -> Talvez v

--Devuelve los valores de los Talvez que no sean Nothing.
fromJusts :: [Talvez a] -> [a]

--Dado un árbol devuelve su elemento mínimo.
minT :: Ord a => Tree a -> Talvez a
-}
