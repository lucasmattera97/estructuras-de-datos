--suponemos una celda de gobstones
--donde cada entero es la representaicon de la cantidad de bolitas
data Celda = C Int Int Int

poner:: Color -> Celda -> Celda
sacar :: Color -> Celda -> Celda
nroBolitas :: Color -> Celda -> Int
celdaVacia :: Celda

{-
A este conjunto de funciones se le puede llamar iterfaz
una interfaz esta formada por el conjunto de operaciones que 
interactuan con, en este caso, celdas . Estas son funciones
que le puedo dar aotro usuario para qu elas use..
A una interfaz se la da una implementacin, la cual depende de 
como elegimos representarla.
Una vez implementada esta interfaz deberia garantiza que todas
las personas que usen el codigo implementen esas operaciones.
El problema aca es que al ser int's en el constructor pudo poner 
cualquier cosa que cuente como int, pero con la interfaz que tengo
yo puedo garantizar que use solo los positivos.

las condiciones que debe cumplir el constructor se les llama
INVARIANTES DE REPRESENTACION. Condiciones que no pueden cambiar 
y siempre tengo que garantizar. En el caso de las celdas el 
invariante es que sea siempre positivo los enteros que recibe.
Los invariantes de prepresentacin son mantenidos por el progra-
mador. Si no se cumplen en el codigo no se da.
Los invariantes son solo mirando el constructor, sin mirar 
la interfaz, sin mirar lo que viene despues.

en haskell hay algo que se llama MODULO al cual va a exportar 
al usuario fuera de la implementacion hacia la interfaz
de panera que solo puede implementar la interfaz en el 
entorno. Hay modulos que dependen de otros modulos.
Un modulo va a exportar la interfaz hacia el archivo en el que
va a estar el usuario y esa interfaz va a vivir solo en ese
archivo

module NombreDelArchivo (nombreFunc, nombreFunc) where
si quiero importar todas las funciones es:
module nombredelarchivo where

import nombreDelConstructor
los constructores se importan y las funciones se traen en modulo

una implementacion eficiente es una implementacion en la que 
todas las funciones son constantes, idealmente
-}

--tenemos que definir el ponern desde cero sin usar ninguna func.
ponerN :: Int -> Color -> Celda -> Celda
ponerN 0 col cel = cel
ponerN n col cel =
	poner col (ponerN (n-1) col cel)

{-
aca yo importe previamente la funcion poner que ya me la dan
y la puedo utilizar
-}