import Queue

ls1 :: Queue Int
ls1 = Q [1,2,3]

ls2 :: Queue Int
ls2 = Q [4,5,6]

--Cuenta la cantidad de elementos de la cola.
-- Q(n)
lengthQ :: Queue a -> Int
lengthQ q = 
	if isEmptyQ q
		then 0
		else 1 + lengthQ (dequeue q) 

{-
Dada una cola devuelve la lista con los mismos elementos,
donde el orden de la lista es el de la cola.
Nota: chequear que los elementos queden en el orden correcto.
-}
queueToList :: Queue a -> [a]
queueToList q = 
	if not (isEmptyQ q)
		then [firstQ q] ++ queueToList (dequeue q)
		else []


--Inserta todos los elementos de la segunda cola en la primera.
unionQ :: Queue a -> Queue a -> Queue a
unionQ q1 q2 =
	if (isEmptyQ q2)
		then q1 
		else queue (firstQ q2) (unionQ q1 (dequeue q2)) 

	

