	--Un Set es un tipo abstracto de datos que consta de las siguientes operaciones:
module Set (Set,
			addS,
			belongS,
			sizeS,
			emptyS
			) where )

data Set a = UnSet [a] Int 

--Crea un conjunto vacío.
emptyS :: Set a
emptyS = UnSet [] 0

--Dados un elemento y un conjunto, agrega el elemento al conjunto.
addS :: Eq a => a -> Set a -> Set a
addS a (UnSet xs n) = UnSet (a:xs) n  

--Dados un elemento y un conjunto indica si el elemento pertenece al conjunto.
belongS :: Eq a => a -> Set a -> Bool
belongS a (UnSet [] _) = True
belongS a (UnSet xs n) = 
	(perten a xs) && (belongS a (UnSet (sigL xs) n))

perten :: Eq a => a -> [a] -> Bool
perten a (x:xs) = a == x

sigL :: [a] -> [a]
sigL [] = []
sigL (x:xs) = xs

--Devuelve la cantidad de elementos distintos de un conjunto.
sizeS :: Eq a => Set a -> Int
sizeS (UnSet xs n) = n

{-
Dados dos conjuntos devuelve un conjunto con todos 
los elementos en común entre ambos.
intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (UnSet xs1 n1) (UnSet xs2 n2)  =
	buscaCoincidencia xs1 xs2 ++ intersectionS (sigL xs1) xs2
-}


{-
--Borra un elemento del conjunto.
removeS :: Eq a => a -> Set a -> Set a

--Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos. conjuntos.
unionS :: Eq a => Set a -> Set a -> Set a
unionS (UnSet xs size) st = 
	addAll xs st

addAll [] st = st
addAll (x:xs) st = addS x (addAll xs st)

buscaCoincidencia :: Eq a => [a] -> [a] -> [a]
buscaCoincidencia [] [] = []
buscaCoincidencia (x:xs) xs2 = concidencia x xs2

--Dado un conjunto devuelve una lista con todos los elementos distintos del conjunto.
setToList :: Eq a => Set a -> [a]
-}

