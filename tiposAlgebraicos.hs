{-elementoEn :: Int -> [a]
elementoEn error = "no hay elementos"
elementoEn [] n = error "no hay elementos"
elementoEn (x:xs) 0 = [x]
elementoEn (x:xs) n = elementoEn xs (n-1)-}

data Dir = 
	Norte
	|Sur
	|Este
	|Oeste
opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Oeste = Este
opuesto Este = Oeste
opuesto Sur = Norte

data Persona = P Int String  deriving (Show,Eq)
--P vendria siendo como un constructor del dato que le paso
--con esto puedo construir tipos con cualquier dato a ingresar pero de un tipo 
--distinto
nombre :: Persona -> String
nombre (P edad nombre) = nombre

crecer :: Persona -> Persona 
--con deriving show me permite poder imprimir en pantalla el dato
--con eq me aseguro de que el tipo de puede comparar
crecer (P edad nombre) = P (edad+1) nombre


data Grupo = G [Persona] deriving Show
cantidad :: Grupo -> Int
cantidad (G xs) = length xs

aumentarAnio :: Grupo -> Grupo
aumentarAnio (G xs) = G (crecerTodos xs)

crecerTodos :: [Persona] -> [Persona]
crecerTodos [] = []
crecerTodos (x:xs) = crecer x : crecerTodos xs 

jorge = P 12 "Jorge"
mariano = P 12 "Mariano"
uno = 1


















