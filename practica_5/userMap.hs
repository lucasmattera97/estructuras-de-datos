import Map
import Set

{-
map1 = assocM (emptyM) 1 2
map2 = assocM (map1) 3 4 
map = assocM map2 1 6
-}

--Implementar como usuario del tipo abstracto Map las siguientes funciones:

--Busca todas las claves dadas en la lista.
buscarClaves :: Eq k => [k] -> Map k v -> [Maybe v]
buscarClaves [] map = []
buscarClaves (k:xs) map = (lookupM map k) : buscarClaves xs map 

--Indica si en el map se encuenrtan todas las claves dadas.
estanTodas :: Eq k => [k] -> Map k v -> Bool
estanTodas [] map = True
estanTodas (x:xs) map = 
	(x == (head (domM map))) && 
	(estanTodas xs 
			(deleteM map 
				(head (domM map)))) 

{-
Actualiza todas las claves dadas por las primeras componentes con los valores de las segundas
componentes de cada par.
-}
actualizarClaves :: Eq k => [(k, v)] -> Map k v -> Map k v
actualizarClaves [] map = map
actualizarClaves (x:xs) map = assocM (actualizarClaves xs map) (fst x) (snd x)

--Une los doms de una lista de maps. En el resultado no hay claves repetidas.
unirDoms :: Eq k => [Map k v] -> [k]
unirDoms [] = []
unirDoms (x:xs) = toList (toSet (domM x ++ unirDoms xs) emptyS)

{-
map1 = M [(1,2), (3,4)]	
map2 = M [(1,6), (7,8)]	
lsMap = [map1, map2]
-}

{-
Dada una lista de claves de tipo k y un mapa que va de k a int, le suma uno a cada número
asociado con dichas claves.
mapSuccM :: Eq k => [k] -> Map k Int -> Map k Int
mapSuccM [] m = m
mapSuccM (k:xs) m = 
	if (elem k (domM m))
		then mapSuccM xs (actualizarClaves [(k, ((lookupM m k)+1))] )
		else mapSuccM xs m
		--por ahi da error porque el map que devuelve actualizar claves no es de tipo k Int sino de tipo k v
-}

{-
{-
Dado dos maps se agregan las claves y valores del primer map en el segundo. Si una clave
del primero existe en el segundo, es reemplazada por la del primero.
Indicar los ordenes de complejidad en peor caso de cada función implementada.
-}
agregarMap:: Eq k => Map k v -> Map k v -> Map k v
-}