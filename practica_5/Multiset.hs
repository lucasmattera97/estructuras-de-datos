module Multiset where
import Map

--data Map k v = M [(k, Int)] deriving(Show)
data MultiSet a = MS (Map a Int) deriving(Show)
--la clave es el elemento y el valor son las ocurrencias de este
{-
map' :: Map k v
map' = assocM map1 1 2	

map1 :: Map k v
map1 = assocM map2 3 4	

map2 :: Map k v
map2 = assocM emptyM 5 6	

mulSet :: MultiSet a
mulSet = MS map' 
-}

--Crea un multiconjunto vacío.
--O(1)
emptyMS :: MultiSet a
emptyMS = MS emptyM

--Dados un elemento y un multiconjunto, agrega una ocurrencia de ese elemento al multiconjunto.

addMS :: Ord a => a -> MultiSet a -> MultiSet a
addMS a (MS map) = MS (assocM map a (sinJust(lookupM map a)+1))

--O(1)
sinJust :: Maybe Int -> Int
sinJust (Just n) = n
sinJust Nothing = 0

{-
Dados un elemento y un multiconjunto indica la cantidad de apariciones de ese elemento en
el multiconjunto.
-}
--O()
ocurrencesMS :: Ord a => a -> MultiSet a -> Int
ocurrencesMS a (MS map) = sinJust (lookupM map a) 
ocurrencesMS a emptyMS = 0

{-
Dados dos multiconjuntos devuelve un multiconjunto con todos los elementos de ambos
multiconjuntos.
-}
--O()
unionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a 
unionMS emptyMS ms = ms
unionMS (MS map1) (MS map2) = MS 


{-
{-
Dados dos multiconjuntos devuelve el multiconjunto de elementos que ambos multiconjuntos
tienen en común.
-}
--O()
intersectionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a (opcional)

{-
Dado un multiconjunto devuelve una lista con todos los elementos del conjunto y su cantidad
de ocurrencias.
-}
--O()
multiSetToList :: MultiSet a -> [(Int,a)]
-}