{-
Implementar las siguientes funciones suponiendo que reciben un árbol binario que cumple los
invariantes de BST. Todas deben ser implementadas en O(log n).
-}
module BST where

data Tree a = EmptyT 
			  | NodeT a (Tree a) (Tree a)


data BinarySearchTree a = BST (Tree a) (Tree a) (Tree a)

{-
root:: BinarySearchTree a --BST (Tree a) (Tree a) (Tree a)	 
root =  NodeT 11 seis diecinueve

seis:: Tree a	 
seis =  NodeT 6 cuatro ocho

diecinueve:: Tree a
diecinueve =  NodeT 19 diecisiete cuarentaytres

cuatro::Tree a
cuatro =  NodeT 4 EmptyT cinco

ocho::Tree a
ocho =  NodeT 8 EmptyT diez

diez:: Tree a
diez =  NodeT 10 EmptyT EmptyT

cinco :: Tree a
cinco = NodeT 5 EmptyT EmptyT

diecisiete :: Tree a
diecisiete =  NodeT 17 EmptyT EmptyT

cuarentaytres:: Tree a
cuarentaytres =  NodeT 43 treintayuno cuarentaynueve

treintayuno:: Tree a
treintayuno =  NodeT 31 EmptyT EmptyT

cuarentaynueve:: Tree a
cuarentaynueve =  NodeT 49 EmptyT 	EmptyT 
-}

--Dado un BST dice si el elemento pertenece o no al árbol.
perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST a EmptyT = False
perteneceBST a (NodeT x ti td) = 
	if a == x
		then True
		else 
			if a < x
				then perteneceBST a ti
				else perteneceBST a td

--Dado un BST dice si el elemento pertenece o no al árbol.
lookupBST :: Ord k => k -> Tree (k, v) -> Maybe v
lookupBST k EmptyT = Nothing
lookupBST k (NodeT (x,v) ti td) = 
	if k == x
		then Just v
		else 
			if k < x
				then lookupBST k ti
				else lookupBST k td


{-
--Dado un BST inserta un elemento en el árbol.
insertBST :: Ord a => a -> Tree a -> Tree a

--Dado un BST devuelve el mínimo elemento.
minBST :: Ord a => Tree a -> a

--Dado un BST devuelve el árbol sin el mínimo elemento
deleteMinBST :: Ord a => Tree a -> Tree a

--Dado un BST devuelve el máximo elemento.
maxBST :: Ord a => Tree a -> a

--Dado un BST devuelve el árbol sin el máximo elemento
deleteMaxBST :: Ord a => Tree a -> Tree a

--Dado un BST borra un elemento en el árbol.
deleteBST :: Ord a => a -> Tree a -> Tree a

--Dado un BST devuelve un par con el mínimo elemento y el árbol sin el mismo.
splitMinBST :: Ord a => Tree a -> (a, Tree a)

--Dado un BST devuelve un par con el máximo elemento y el árbol sin el mismo.
splitMaxBST :: Ord a => Tree a -> (a, Tree a)

{-
Dado un árbol cualquiera indica si cumple con las condiciones de BST. Nota: está función no
se suele usar en ninguna implementación porque es costosa.
-}
esBST :: Ord a => Tree a -> Bool

{-
Dado un BST y un elemento, devuelve el máximo elemento que sea menor o igual al elemento
dado.
-}
elMaximoMenorA :: Ord a => a -> Tree a -> Maybe a

{-
Dado un BST y un elemento, devuelve el mínimo elemento que sea mayor o igual al elemento
dado.
-}
elMinimoMayorA :: Ord a => a -> Tree a -> Maybe a
-}
