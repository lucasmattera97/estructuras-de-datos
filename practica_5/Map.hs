module Map where

data Map k v = M [(k, v)] deriving(Show)

map = M [(1,2), (3,4)]	

--La interfaz del tipo abstracto Map es la siguiente:
--O(1)
emptyM :: Map k v
emptyM = M []


--supongo que me dan un map, un elemento k para poner en clave, un elemento v para poner en valor,
--asociar ambos elementos y agregarlos al map
--O(n)
assocM :: Eq k => Map k v -> k -> v -> Map k v
assocM (M ls) k v = M (assocL ls k v) 

assocL :: Eq k => [(k,v)] -> k -> v -> [(k,v)]
assocL [] k v =  [(k,v)]
assocL (x:xs) k v =
	if k == (fst x)
		then (k, v) : xs
		else x : (assocL xs k v) 


--devuelve el valor que puede o no encontrarse en el map
--O(n)
lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM (M xs) k = coincidencia k xs

coincidencia :: Eq k => k -> [(k,v)] -> Maybe v
coincidencia k [] = Nothing
coincidencia k (tupla:tuplas) =
	if k == (fst tupla)
		then Just (snd tupla)
		else coincidencia k tuplas

--O(n)
deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (M xs) k = M (borrarTuple k xs)

borrarTuple :: Eq k => k -> [(k,v)] -> [(k,v)]
borrarTuple  k [] = []
borrarTuple k (tupla:xs) = 
	if k /= (fst tupla)    
		then tupla : (borrarTuple k xs)
		else borrarTuple k xs	

--O(n)
domM :: Map k v -> [k]
--devuelve todas las claves de un map
domM (M []) = []
domM (M xs) = clavesDe xs

clavesDe :: [(k,v)] -> [k]
clavesDe [] = []
clavesDe (tupla:xs) = fst tupla : (clavesDe xs) 

------------------------------------------------------------------
{-
deleteM_alt :: Map k v = k -> [v]
deleteM_alt (M ks vs) k = armarMapa(eliminar ks vs k)

armarMapa :: ([k], [v]) -> Map k v
armarMapa ks vs = M ks vs

eliminar :: [k] -> [v] -> k -> ([k], [v])
eliminar (k ks) (v vs) b =
	if k == b
		then (ks, vs)
		else func x v (eliminar ks vs b)

func :: k -> v -> ([k], [v]) -> ([k], [v])
func k v (ks, vs) = (k:ks, v:vs)
-}