	--Un Set es un tipo abstracto de datos que consta de las siguientes operaciones:
module Set where 

data Set a = S [a] Int  deriving(Show)

set = S [1,2,3] 3

--Crea un conjunto vacío.
--O(1)
emptyS :: Set a
emptyS = S [] 0

--Dados un elemento y un conjunto, agrega el elemento al conjunto.
--O(1)
addS :: Eq a => a -> Set a -> Set a
addS a (S xs n) = 
	if not (elem a xs) 
		then S (a:xs) (n+1)
		else (S xs n)  

--Dados un elemento y un conjunto indica si el elemento pertenece al conjunto.
--O(n)
belongS :: Eq a => a -> Set a -> Bool
belongS a (S [] _) = True
belongS a (S xs n) = 
	(elem a xs) && (belongS a (S (sigL xs) n))

sigL :: [a] -> [a]
sigL [] = []
sigL (x:xs) = xs

--Devuelve la cantidad de elementos distintos de un conjunto.
--O(1)
sizeS :: Eq a => Set a -> Int
sizeS (S xs n) = n

toSet :: Eq a => [a] -> Set a -> Set a
toSet [] s = s
toSet (x:xs) s = addS x (toSet xs s)  

toList :: Set a -> [a]
toList (S (x:xs) n) = x : (toList (S xs n))  
toList emptyS = []
	
{-
Dados dos conjuntos devuelve un conjunto con todos 
los elementos en común entre ambos.
intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (S xs1 n1) (S xs2 n2)  =
	buscaCoincidencia xs1 xs2 ++ intersectionS (sigL xs1) xs2
-}


{-
--Borra un elemento del conjunto.
removeS :: Eq a => a -> Set a -> Set a

--Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos. conjuntos.
unionS :: Eq a => Set a -> Set a -> Set a
unionS (S xs size) st = 
	addAll xs st

addAll [] st = st
addAll (x:xs) st = addS x (addAll xs st)

buscaCoincidencia :: Eq a => [a] -> [a] -> [a]
buscaCoincidencia [] [] = []
buscaCoincidencia (x:xs) xs2 = concidencia x xs2

--Dado un conjunto devuelve una lista con todos los elementos distintos del conjunto.
setToList :: Eq a => Set a -> [a]
-}

