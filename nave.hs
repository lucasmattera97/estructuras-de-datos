--HOLA HOLA

data Contenedor = 
	Comida 
	| Oxigeno 
	| Torpedo 
	| Combustible deriving(Show)

data Componente = 
	Escudo 
	| CanionLaser 
	| Lanzatorpedos 
	| Motor Int 
	| Almacen [Contenedor] deriving(Show)

data Nave = 
	Parte Componente Nave Nave 
	| ParteBase deriving(Show)

{-Ejercicio 1 Construya un valor de tipo Nave que contenga 
un motor, check
un escudo, check
dos armas y check
un almacen que posea comida.-}

nave :: Nave
nave = Parte CanionLaser (naveCanionLaser) (naveMotor)

naveCanionLaser :: Nave
naveCanionLaser = Parte CanionLaser (naveEscudo) (naveAlmacenComida)

naveMotor :: Nave
naveMotor = Parte (Motor 4) ParteBase ParteBase

naveEscudo :: Nave
naveEscudo = Parte Escudo ParteBase ParteBase

naveAlmacenComida :: Nave
naveAlmacenComida = Parte (Almacen [Comida]) ParteBase ParteBase

{-
a) 
Retorna la lista de componentes.
Ejemplo:
componentes 
	(Parte Escudo ParteBase 
		(Parte Lanzatorpedos ParteBase ParteBase) => [Escudo, Lanzatorpedos]
-}
componentes :: Nave -> [Componente]
componentes ParteBase = []
componentes (Parte comp n1 n2) = comp : (componentes n1 ++ componentes n2)

{-
b)
Retorna el poder de propulsion de una nave. 
El poder de propulsion de una nave es la suma de los poderes de 
propulsion de los motores de la nave.
Ejemplo:
poderDePropulsion 
	(Parte Lanzatorpedos 
		
		(Parte (Motor 5) 
			ParteBase 
			ParteBase)
		
		(Parte (Motor 20) 
			ParteBase 
			ParteBase) 			=> 25
-}

navePropulsion =
	(Parte Lanzatorpedos 
		
		(Parte (Motor 5) 
			ParteBase 
			ParteBase)
		
		(Parte (Motor 20) 
			ParteBase 
			ParteBase))

poderDePropulsion :: Nave -> Int
poderDePropulsion ParteBase = 0
poderDePropulsion (Parte comp n1 n2) = 
	poderDeMotor comp + (poderDePropulsion n1) + (poderDePropulsion n2)

poderDeMotor :: Componente -> Int
poderDeMotor (Motor poder) = poder
poderDeMotor _ = 0


{-
c) 
Reemplaza armas por escudos.
Ejemplo:
desarmarse 

	(Parte Lanzatorpedos
		(Parte CanionLaser 
			ParteBase 
			ParteBase)

		(Parte (Motor 20) 
			ParteBase 
			ParteBase)

		=>
	(Parte Escudo
		(Parte Escudo 
			ParteBase 
			ParteBase)

	(Parte (Motor 20) 
		ParteBase 
		ParteBase)
-}

naveArmada = (Parte Lanzatorpedos
		(Parte CanionLaser 
			ParteBase 
			ParteBase)

		(Parte (Motor 20) 
			ParteBase 
			ParteBase))

desarmarse :: Nave -> Nave
desarmarse ParteBase = ParteBase
desarmarse (Parte Lanzatorpedos n1 n2) = (Parte Escudo (desarmarse n1) (desarmarse n2)) 
desarmarse (Parte CanionLaser n1 n2) = (Parte Escudo (desarmarse n1) (desarmarse n2))
desarmarse (Parte c n1 n2) = (Parte c (desarmarse n1) (desarmarse n2))

{-
d)
Dada una nave devuelve la cantidad de comida. Cada aparicion de Comida vale 1.
Ejemplo:
cantidadComida 
	(Parte Lanzatorpedos
		(Parte (Almacen [Combustible, Comida, Comida]) 
			ParteBase 
			ParteBase)

		(Parte (Almacen [Torpedo, Comida, Oxigeno]) 
			ParteBase 
			ParteBase)  	=> 3
-}
naveComida :: Nave
naveComida = 

	(Parte Lanzatorpedos
		(Parte 
			(Almacen 
				[Combustible, 
				Comida, 
				Comida]) 
			ParteBase 
			ParteBase)

		(Parte 
			(Almacen 
				[Torpedo, 
				Comida, 
				Oxigeno]) 
			ParteBase 
			ParteBase))

cantidadComida :: Nave -> Int
cantidadComida ParteBase = 0
cantidadComida (Parte componente n1 n2) = 
	tieneComida componente  + cantidadComida n1 + cantidadComida n2
--cantidadComida (Parte _ n1 n2) = 0 +  cantidadComida n1 + cantidadComida n2

tieneComida :: Componente -> Int
--tieneComida (Almacen [])  0
tieneComida (Almacen (x:xs)) = esComida x
tieneComida _ = 0 

esComida :: Contenedor -> Int
esComida Comida = 1
esComida _ = 0

{-
f) Dada una lista de contenedor 
chequea que cada almac ́en contenga todos esos tipos de contenedores.
-}
aprovisionados :: [Contenedor] -> Nave -> Bool
aprovisionados [] n = True
aprovisionados (x:xs) nave = 
	if esAlmacen nave 
		then (apareceEnLaNave x nave) && (aprovisionados xs nave) 
		else  aprovisionados xs nave
aprovisionados _ _ = False

esAlmacen :: Nave -> Bool
esAlmacen ParteBase = False
esAlmacen (Parte (Almacen xs) n1 n2) = True
esAlmacen (Parte _ _ _) = False

--precondicion: la nave tiene un almacen
apareceEnLaNave :: Contenedor -> Nave -> Bool
apareceEnLaNave c ParteBase = False
apareceEnLaNave c1 (Parte c2 n1 n2) = --en c2 va un componente 
	(esIgual c1 c2) || (apareceEnLaNave c1 n1) || (apareceEnLaNave c1 n2)

--siempre recibe un Almacen
esIgual :: Componente -> Contenedor -> Bool
esIgual Oxigeno Oxigeno = True
esIgual Comida Comida = True
esIgual Torpedo Torpedo = True
esIgual Combustible Combustible = True
esIgual _ _ = False



