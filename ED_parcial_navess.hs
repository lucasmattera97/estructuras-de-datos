data Contenedor = Comida 
                | Oxigeno 
                | Torpedo 
                | Combustible

data Componente = Escudo 
                | CanionLaser 
                | Lanzatorpedos 
                | Motor Int 
                | Almacen [Contenedor]

data Nave = Parte Componente Nave Nave 
            | ParteBase

{-Construya un valor de tipo Nave que contenga un motor, un escudo, dos armas y un almacen que posea comida.-}
nave::Nave
nave = Parte (Motor 4) naveEscudo naveArmasAlmacen

naveEscudo::Nave
naveEscudo = Parte (Escudo) ParteBase ParteBase

naveArmasAlmacen::Nave
naveArmasAlmacen = Parte CanionLaser naveArma naveAlmacen

naveArma::Nave
naveArma = Parte Lanzatorpedos ParteBase ParteBase

naveAlmacen::Nave
naveAlmacen = Parte Almacen[Comida] ParteBase ParteBase

{-a) Retorna la lista de componentes.-}
componentes :: Nave -> [Componente]
componentes ParteBase = []
componentes (Parte c n1 n2) = 
    c : (componentes n1 ++ componentes n2)

{-b) Reemplaza armas por escudos.-}
desarmarse :: Nave -> Nave
desarmarse ParteBase = ParteBase
desarmarse (Parte CanionLaser n1 n2) = Parte Escudo (desarmarse n1) (desarmarse n2)
desarmarse (Parte Lanzatorpedos n1 n2) = Parte Escudo (desarmarse n1) (desarmarse n2)
desarmarse (Parte c n1 n2) = Parte c (desarmarse n1) (desarmarse n2)

{-
    if c == CanionLaser
        then Parte Escudo (desarmarse n1) (desarmarse n2)
    else 
        if c == Lanzatorpedos
            then Parte Escudo (desarmarse n1) (desarmarse n2)
        else 
            Parte c (desarmarse n1) (desarmarse n2)
-}

{-d) Dada una nave devuelve la cantidad de comida. Cada aparicion de Comida vale 1.-}
cantidadComida :: Nave -> Int
cantidadComida ParteBase = 0
cantidadComida (Parte c n1 n2) = (f1 c) + (cantidadComida n1) + (cantidadComida n2)

f1 :: Componente -> Int
f1 (Almacen xs) = (f2 xs) 
f1 c = 0

f2 :: [Contenedor] -> Int 
f2 (x:xs) = 
    if x == Comida
        then 1 + (f2 xs)
    else 
        f2 xs

        
{-f) Dada una lista de contenedor chequea que cada almac ́en contenga todos esos tipos de contenedores.-}
aprovisionados :: [Contendor] -> Nave -> Bool
aprovisionados [] n = true

    
        