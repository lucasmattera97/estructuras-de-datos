data EscuelaDeMagia = EDM (Set Hechizo) (Map Nombre Mago) (MaxHeap Mago)

{-
Invariantes de representacion: 
- todos los magos del map se encuentran en la maxHeap de la escuela
- los magos del map no pueden saber hechizos que no se hallan enseniado, o sa que no pertenezcan al Set
-}


{- 
b - devuelve una escuela vacia
O(1)
-}
fundarEscuela :: EscuelaDeMagia
fundarEscuela = EDM emptyS emptyM emptyH


{-
c - indica si la escuela esta vacia
O(1)
-}
estaVacia :: EscuelaDeMagia -> Bool
estaVacia (EDM s m mh) = isEmptyH mh


{-
d - incorpora un mago a la escuela (si ya existe no hace nada)
O(log M)
-}
registrar :: Nombre -> EscuelaDeMagia -> EscuelaDeMagia
registrar n (EDM s m mh) = 
    if elem n (domM m)  --correccion del profe: esto es caro
        then EDM s m mh
        else EDM s (assocM n (crearM n) m) (insertH (crearM n) mh)


{-
e - devuelve los nombres de los magos registrados en la escuela 
O(log M)
-}
magos :: EscuelaDeMagia -> [Nombre]
magos fundarEscuela = []
magos (EDM _ m _) = domM m


{-
f - devuelve los hechizos que conoce un mago dado
prec: existe un mago con dicho nombre
O(log M)
-}
hechizosDe :: Nombre -> EscuelaDeMagia -> Set Hechizo
hechizosDe n (EDM s m mh) = spellsFromM n (domM m) m

{-
proposito: idem proposito hechizoDe
hechizos O(1)
lookupM O(lok k)
spellsFromM O(log k)
-}
spellsFromM :: Nombre -> [Nombre] -> Map Nombre Mago -> Set Hechizo
spellsFromM n (x:xs) m =
    if n == x
        then hechizos(fromJust(lookupM x m))
        else spellsFromM n xs m



{-
g - dado un mago indica la cantidad de hechizos que la escuela ha dado y el no sabe
prec.: existe un mago con dicho nombre
O(log M)
-}
leFaltanAprender :: Nombre -> EscuelaDeMagia -> Int
leFaltanAprender n em = (sizeS(hechizosSH em)) - (sizeS(hechizosDe n em))

{-
proposito: devuelve todos los hechizos que se hayan enseniado en la escuela
O(1)
-}
hechizosSH :: EscuelaDeMagia -> Set Hechizo
hechizosSH fundarEscuela = emptyS
hechizosSH (EDM s _ _) = s


{-
h - devuelve el mago que mas hechizos sabe y la escuela sin dicho mago
pec.: hay al menos un mago 
O(log M)
-}
egresarUno :: EscuelaDeMagia -> (Mago, EscuelaDeMagia)
egresarUno em = 
    (
        masSabe em, 
        sinMago (masSabe em) em
    )

{-
proposito: denota el mago que mas hechizos sabe de la escuela
precondicion: hay al menos uno
O(1)
-}
masSabe :: EscuelaDeMagia -> Mago
masSabe (EDM _ _ mh) = maxH mh

{-
proposito: denota la escuela sin el mago dado
precondicion: existe el mago en la escuela y el mago
es el que mas sabe
nombre O(1)
deleteM O(log k)
deleteMaxH O(log M)
sinMago O(log k + log M)
-}
sinMago :: Mago -> EscuelaDeMagia -> EscuelaDeMagia
sinMago m (EDM s map mh) = 
        EDM
            s
            (deleteM (nombre m) map)
            (deleteMaxH mh)


{-
i - ensenia un hechizo a un mago existente y si el hechizo no existe en la escuela
es incorporado en la misma
nota: no importa si el mago ya conoce el hechizo dado
prec.: existe un mago con dicho nombre
O(M log M + log H)
-}
enseniar :: Hechizo -> Nombre -> EscuelaDeMagia -> EscuelaDeMagia
enseniar h n (EDM s m mh) = 
    EDM
        (add h s)
        (conMagoEnseniado h n m)
        (mHActual h n m mh)

{-
proposito: agrega el mago con le hechizo aprendido al map dado
precondicon: existe el mago
assocM O(log k)
aprender O(log h)
lookupM O(log k)
conMagoEnseniado O(log k + log h)
-}
conMagoEnseniado h n m = 
    assocM n 
            (
                aprender h (fromJust (lookupM n m))
            )


{-
------------------------------U S E R----------------------------------------
-}
{-
j - retorna todos los hechizo aprendidos por los magos
o(M * (log M + H log H))
-}
hechizosAprendidos :: EscuelaDeMagia -> Set Hechizo

{-
k - indica si existe un mago que sabe toods los hechizoa eseniados por la escuela
O(log M)
-}
hayUnExperto :: EscuelaDeMagia -> Bool

{-
l - devuelve un par con la lista de magos que saben todos los hechizos dados por la escuela y la escuela sin dichos magos
O(M log M)
-}
egresarExpertos :: EscuelaDeMagia -> ([Mago], EscuelaDeMagia)

{-
bonus - dar una posible representacion para le tipo mago
de manera de que se pueda cumplir con el orden dado para c/operacion de la 
interfaz pero sin implementarlas
-}

