type Software = String --es lo mismo que decir que software es de tipo string
data Autor = Dev String | Admin String
data Organizador = Agregar Software [Autor] Organizador | Vacio

pares :: Organizador ->[(Software, Int)]
pares vacio = []
pres (Agregar n as orgs) =
	(n,length as) : (pares orgs)

enComun :: Autor -> Autor -> Organizador -> Bool
enComun a1 a2 =
	enComun a1 a2 (Agregar n as orgs) =
		((pertenece a1 as) || (enComun a1 a2 orgs) && (pertenece a2 as))

--saca cada autor de la lista de la lista de cada software en la que aparezca
filtrar :: [Autor] -> Organizador -> Organizador
filtrar as1 [] org = org
filtrar (a:as) org =
	sacarA a (filtrar as org)

sacarA :: Autor -> Organizador -> Organizador
sacarA a Vacio = Vacio
sacarA a (Agregar n as orgs) = Agregar n (delete a as) (sacarA a orgs)

losAdmin :: Organizador -> [Autor]
losAdmin Vacio = 
losAdmin (Agregar n as orgs) = agregarAdmins as (losAdmin orgs)

agregarAdmins :: [Autor] -> [Autor] ->[Autor]
agregarAdmins [] r = r
agregarAdmins (a:as) r = 
	if esAdmin a && not(pertenece r)
		then a : (agregarAdmins as r)
		else agregarAdmins as r

esAdmin :: Autor -> Bool
esAdmin (Admin n) = True
esAdmin x = False

--ordena los software de menos a mayor por cantidad de desarolladores
ordenados :: Organizador -> [(Software, Int)]
ordenados org = ordenar(pares org)

ordenar :: [(Software, Int)] -> [(Software, Int)]
ordenar [] = []
ordenar (p:ps) = insertar p (ordenar ps)

insertar :: (Software ,Int) -> [(Software, Int)] -> [(Software, Int)]
insertar x [] = [x]
insertar x (p:ps) =
	if snd x > snd p 
	then p : insertar x ps
		else x : p : ps

