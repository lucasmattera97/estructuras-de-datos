data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)
--data Tree a = EmptyT | NodeT Int (Tree Int) (Tree Int) deriving (Show)
 
raiz51 = NodeT 51 (rama12) (rama87)

--rama izquierda de raiz
rama12 = NodeT 12 (rama1) (rama43) 

--rama derecha de raiz
rama87 = NodeT 87 (rama52) EmptyT

--rama de 87
rama52 = NodeT 52 EmptyT (rama83)
rama83 = NodeT 83 EmptyT EmptyT

--rama derecha de 87
rama1 = NodeT 1 EmptyT EmptyT
rama43 = NodeT 43 (rama36) EmptyT

--rama de 43
rama36 = NodeT 36 EmptyT EmptyT

--1. Dado un árbol binario de enteros devuelve la suma entre sus elementos.
sumarT :: Tree Int -> Int
sumarT EmptyT = 0
sumarT (NodeT n tl tr) = n + (sumarT tl) + (sumarT tr)

{-2. Dado un árbol binario devuelve su cantidad de elementos, es decir, el tamaño del árbol (size
en inglés).-}
sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT (NodeT a tl tr) = 1 + (sizeT tl) + (sizeT tr)

--3. Dado un árbol de enteros devuelve un árbol con el doble de cada número.
mapDobleT :: Tree Int -> Tree Int
mapDobleT EmptyT = EmptyT
mapDobleT (NodeT n tl tr) = NodeT (n*2) (mapDobleT tl) (mapDobleT tr) 

--4. Dado un árbol de palabras devuelve un árbol con la longitud de cada palabra.
mapLongitudT :: Tree String -> Tree Int
mapLongitudT EmptyT = EmptyT
mapLongitudT (NodeT p tl tr) = NodeT (length p) (mapLongitudT tl) (mapLongitudT tr)

raizPalabra = NodeT "a" (ramaPalabra) EmptyT
ramaPalabra = NodeT "enorn" EmptyT EmptyT

{-5. Dados un elemento y un árbol binario devuelve True si existe un elemento igual a ese en el
árbol.-}
perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT a EmptyT = False
perteneceT a (NodeT b tl tr) = (a == b) || (perteneceT a tl) || (perteneceT a tr)

{-6. Dados un elemento e y un árbol binario devuelve la cantidad de elementos del árbol que son
iguales a e.-}
aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT a EmptyT = 0
aparicionesT a (NodeT b tl tr) = 
	if a == b 
		then 1 + (aparicionesT a tl) + (aparicionesT a tr)
		else (aparicionesT a tl) + (aparicionesT a tr)

{-7. Dado un árbol devuelve su cantidad de hojas.
Nota: una hoja (leaf en inglés) es un nodo que no tiene hijos.-}
countLeaves :: Tree a -> Int
countLeaves EmptyT = 0
countLeaves (NodeT a EmptyT EmptyT) = 1 
countLeaves (NodeT a tl tr) = countLeaves tl + countLeaves tr

--8. Dado un árbol devuelve los elementos que se encuentran en sus hojas.
leaves :: Tree a -> [a]
leaves EmptyT = [] 
leaves (NodeT a EmptyT EmptyT) = [a]
leaves (NodeT a tl tr) = leaves tl ++ leaves tr

{-9. Dado un árbol devuelve su altura.
Nota: la altura de un árbol (height en inglés), también llamada profundidad, es la cantidad
de niveles del árbol (También existen otras definiciones posibles. Por ejemplo, puede definirse 
como la distancia del camino desde la raíz a su hoja más lejana. 
Por distancia entendemos la cantidad de nodos que hay en dicho camino. 
En este caso las hojas tendrían altura 0, porque la distancia del camino a sí mismos lo es. 
Se suele utilizar más en árboles que no poseen un constructor vacío). 
La altura de un árbol vacío es cero y la de una hoja es 1.-}
heightT :: Tree a -> Int
heightT EmptyT = 0
heightT (NodeT a EmptyT EmptyT) = 1
heightT (NodeT a tl tr) = 1 + (max (heightT tl) (heightT tr))

{-10. Dado un árbol devuelve el número de nodos que no son hojas. ¿Cómo podría resolverla sin
utilizar recursión explícita? Primero defínala con recursión explícita y después sin ella.-}
countNotLeaves :: Tree a -> Int
countNotLeaves EmptyT = 0
countNotLeaves (NodeT a EmptyT EmptyT) = 0
countNotLeaves (NodeT a tl tr) = 1 + (countNotLeaves tl) + (countNotLeaves tr)

{-11. Dado un árbol devuelve el árbol resultante de intercambiar el hijo izquierdo con el derecho,
en cada nodo del árbol.-}
mirrorT :: Tree a -> Tree a
mirrorT EmptyT = EmptyT
mirrorT (NodeT a tl tr) = NodeT a tr tl

{-12. Dado un árbol devuelve una lista que representa el resultado de recorrerlo en modo in-order.
Nota: En el modo in-order primero se procesan los elementos del hijo izquierdo, luego la raiz
y luego los elementos del hijo derecho-}
listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT a tl tr) = listInOrder tl ++ [a] ++ listInOrder tr

{-13. Dado un árbol devuelve una lista que representa el resultado de recorrerlo en modo pre-order.
Nota: En el modo pre-order primero se procesa la raiz, luego los elementos del hijo izquierdo,
a continuación los elementos del hijo derecho.-}
listPreOrder :: Tree a -> [a]
listPreOrder EmptyT = []
listPreOrder (NodeT a tl tr) = [a] ++ listPreOrder tl ++ listPreOrder tr

{-14. Dado un árbol devuelve una lista que representa el resultado de recorrerlo en modo postorder.
Nota: En el modo post-order primero se procesan los elementos del hijo izquierdo, 
a continuación los elementos del hijo derecho y finalmente la raiz.-}
listPosOrder :: Tree a -> [a]
listPosOrder EmptyT = []
listPosOrder (NodeT a tl tr) = listPosOrder tl ++ listPosOrder tr ++ [a]

{-15. Dado un árbol de listas devuelve la concatenación de todas esas listas. El recorrido debe ser
in-order. 
Nota: En el modo in-order primero se procesan los elementos del hijo izquierdo, luego la raiz
y luego los elementos del hijo derecho-}
concatenarListasT :: Tree [a] -> [a]
concatenarListasT EmptyT = []
concatenarListasT (NodeT a  tl tr) = concatenarListasT tl ++ a ++ concatenarListasT tr

raiz51Lista = NodeT [51] (rama12Lista) (rama87Lista)

--rama izquierda de raiz
rama12Lista = NodeT [12] (rama1Lista) (rama43Lista) 

--rama derecha de raiz
rama87Lista = NodeT [87] (rama52Lista) EmptyT

--rama de 87
rama52Lista = NodeT [52] EmptyT (rama83Lista)
rama83Lista = NodeT [83] EmptyT EmptyT

--rama derecha de 87
rama1Lista = NodeT [1] EmptyT EmptyT
rama43Lista = NodeT [43] (rama36Lista) EmptyT

--rama de 43
rama36Lista = NodeT [36] EmptyT EmptyT

{-16. Dados un número n y un árbol devuelve una lista con los nodos de nivel n. El nivel de un
nodo es la distancia que hay de la raíz hasta él. La distancia de la raiz a sí misma es 0, y la
distancia de la raiz a uno de sus hijos es 1.
Nota: El primer nivel de un árbol (su raíz) es 0.-}
levelN :: Int -> Tree a -> [a]  	
levelN n EmptyT = []
levelN n (NodeT a tl tr) = 
	if (n > 0)
		then (levelN (n-1) tl) ++ (levelN (n-1) tr)
		else [a]

--17. Dado un árbol devuelve una lista de listas en la que cada elemento representa 
--un nivel de dicho árbol.
listPerLevel :: Tree a -> [[a]]		--no anda
listPerLevel EmptyT = []
listPerLevel (NodeT a tl tr) = [a] : (unir (listPerLevel tl) (listPerLevel tr))

--func.a aux.
unir :: [[a]] -> [[a]] -> [[a]]
unir xs [] = xs
unir [] xs = xs
unir (xs:xss) (ys:yss) = (xs++ys) : unir xss yss 

--18. Devuelve los elementos de la rama más larga del árbol
--ramaMasLarga :: Tree a -> [a]

--19. Dado un árbol devuelve todos los caminos, es decir, los caminos desde la raiz hasta las hojas.
todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT a EmptyT EmptyT) = [[a]]
todosLosCaminos (NodeT a tl tr) = agregarATodas a ((todosLosCaminos tl) ++ (todosLosCaminos tr))

agregarATodas :: a -> [[a]] -> [[a]]
agregarATodas a [] = []
agregarATodas a (xs:xss) = (a:xs) : agregarATodas a xss

{-
{-20. Indica si el árbol está balanceado. Un árbol está balanceado cuando para cada nodo la
diferencia de alturas entre el subarbol izquierdo y el derecho es menor o igual a 1.-}
balanceado :: Tree a -> Bool
-}