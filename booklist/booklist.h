

struct BooklistSt;
typedef BooklistSt* Booklist;

void addBook(Book book, BookList bookList);
BookList mkBookList();
Book takeOne(BookList bookList);
void mergeBookList(BookList dest, BookList source)
