data Manada = M Lobo
data Lobo = Cria Nombre 
			|Explorador Nombre [Territorio] Lobo Lobo
			|Cazador Nombre [Presa] Lobo Lobo Lobo
type Nombre = String
type Presa = String
type Territorio = String

--EJERCICIO 1
manada = M loboCaza
loboCaza = Cazador "tango" ["siervo", "conejo"] loboExp1 loboExp2 cria1
lobo loboExp1 = Explorador "eco" ["bosque", "rio"] cria2 cria3
loboExp2 = Explorador "alfa" ["montania", "lago"] cria4 cria5
cria1 = Cria "A"
cria2 = Cria "B"
cria3 = Cria "C"
cria4 = Cria "D"
cria5 = Cria "E"

-- EJERCICIO 2
buenaCaza :: Manada -> Bool
buenaCaza (M lobo) = alimentoTotal lobo > criasTotal lobo

alimentoTotal :: Lobo -> Int
alimentoTotal (Crias n) = 0
alimentoTotal (Explorador _ ts l1 l2) =
	if esCazador l1 
		then cantPresas l1 + alimentoTotal l2
		else alimentoTotal l1 + alimentoTotal l2
alimentoTotal (Cazador _ ps l1 l2 l3) = 
	lenght ps + alimentoTotal l1 + alimentoTotal l2 + alimentoTotal l3

criasTotal :: Lobo -> Int
criasTotal (Cria _) = 1
criasTotal (Cazador _ _ l1 l2 l3) = criasTotal l1 + criasTotal l2 + criasTotal l3
criasTotal (Explorador _ _ l1 l2) = criasTotal l1 + criasTotal l2

--EJERCICIO 3
elAlfa :: Manada -> (Nombre, Int)
elAlfa (M l) = elAlfa' l

elAlfa' :: Lobo -> (Nombre, Int)
elAlfa' (Cria _) = (_, 0)
elAlfa' (Explorador n _ l1 l2) =
	if (not (esCazador l1)) && (not (esCazador l2))
		then (n, 0)
		else elMejorDe2 (elAlfa' l1) (elAlfa' l2)
elAlfa' (Cazador n ps l1 l2 l3) = elMejorDe3 (n, ps) (elAlfa' l1) (elAlfa' l3) --no llamo a l2

elMejorDe2 :: (Nombre, Int) -> (Nombre, Int) -> (Nombre, Int)
elMejorDe2 (a,0) (b,0) = (a,0)
elMejorDe2 (a,x1) (b,x2) =
	if x1 > x2 
		then (a,x1)
		else (b,x2)

elMejorDe3 :: (Nombre, Int) -> (Nombre, Int) -> (Nombre, Int) -> (Nombre, Int) 
elMejorDe3 (a,0) (b,0) (c,0) = (a,0)
elMejorDe3 (a,x1) (b,x2) (c,x3) = 
	elMejorDe2 (
		elMejorDe2 (a,x1)
		elMejorDe2 (b,x2)) --le paso un solo parametro a la func.
							--tendria que haber usado la func. dos veces 		
		(elMejorDe2 (b,x2)
		elMejorDe2 (c,x3))

--EJERCICIO 3
losQueExploraron :: Territorio -> Manada -> [Nombre]
losQueExploraron _ m =
	if esCria m --se podria haber hecho con pattern matching 
		then []
		else losQueExploraron _ m
	if esCazador m
		then [] ++ losQueExploraron _m --falta el else
	if esExplorador m
		then losQueExploraronExploradores _ m

losQueExploraronExploradores :: Territorio -> Lobo -> [Nombre]
--precondicion: solo recibe un explorador
losQueExploraronExploradores t (Explorador n ts l1 l2) =
	pasoPorAca n t ts ++ 
	losQueExploraronExploradores l1 ++
	losQueExploraronExploradores l2

pasoPorAca :: Nombre -> Territorio -> [Nombre]
pasoPorAca _ _ [] = []
pasoPorAca n t (x:xs) =
	if t == x
		then [n]
		else pasoPorAca n t xs


--EJERCICIO 4 
subordinadosCazador :: Nombre -> Manada -> [Nombre]
subordinadosCazador n (M l) =
	if esCria 		'					--denuevo' el uso del if en vez de PM
		then [] ++ subordinadosCazador' n l
		else subordinadosCazador' n l
	if esExplorador
		then [] ++ subordinadosCazador' n l 
		else subordinadosCazador' n l
	if esCazador
		then 
			if n == nombre m
				then getCazadoresSubordinadosMAN n m
				else subordinadosCazador' n l
		else subordinadosCazador' n l

subordinadosCazador' :: Nombre -> Lobo -> [Nombre]
subordinadosCazador' (Cria _) = []
subordinadosCazador' (Explorador _ _ l1 l2) = 
	[] ++ (subordinadosCazador' l1) ++ (subordinadosCazador' l2)
subordinadosCazador' (Cazador _ _ l1 l2 l3) =
	if np == n
		then getCazadoresSubordinadosLOBO n l
		else subordinadosCazador' np l1 ++
			subordinadosCazador' np l2 ++
			subordinadosCazador' np l3

getCazadoresSubordinadosLOBO :: Nombre -> Lobo -> [Nombre]
getCazadoresSubordinadosLOBO n (Cria _) = [] --no hay recursion
getCazadoresSubordinadosLOBO n (Explorador _ _ l1 l2) =
	[] ++ getCazadoresSubordinadosLOBO l1 ++
	getCazadoresSubordinadosLOBO l2
getCazadoresSubordinadosLOBO (Cazador n _ l1 l2 l3) =
	n : (getCazadoresSubordinadosLOBO l1 ++
		getCazadoresSubordinadosLOBO l2 ++
		getCazadoresSubordinadosLOBO l3)

getCazadoresSubordinadosMAN :: Nombre -> Manada -> [Nombre]
getCazadoresSubordinadosMAN (M l) = getCazadoresSubordinadosLOBO n l








