{-

¿25?

ls =[10,5,1,50,25,100]
ls_ord = [1,2,10,25,50,100]


-}

data Tree a = EmptyT 
			  | NodeT a (Tree a) (Tree a)

data Map k v = M(Tree(k,v))

assoc :: k -> v -> Tree (k,v) -> Tree(k,v)
assoc k v EmptyT = NodeT (k,v) EmptyT EmptyT
assoc k v (NodeT (kt, vt) ti td) = 
	if k == kt
		then NodeT (kt, vt)

--que pasa si quiero balancear un arbol con maps
--AVL son unas siglas que cuando se dice que la impolementa un arbol significa que éste
--arbol va a estar balanceado

--recibe un elemento y dos AVL y devuelve un AVL
chequear :: a -> Tree a -> Tree a -> Tree a
chequear x ti td =
	if abs (height ti - height td) =<  --abs es la funcion valor absoluto
		then NodeT x ti td
		else 
			if height td > height ti
				then rotarIzq x ti td
				else rotarDer x ti td

rotarIzq :: x -> ti -> Tree a
rotarIzq x ti (NodeT cd tdi tdd) = 
	if height tdd > height tdi --si es asi la rotacion es simple, rota hacia un lado siempre
		then NodeT xd (NodeT x ti tdi) tdd
		else...  

rotarDer :: x -> ti -> (NodeT cd tdi tdd)

armar :: x -> ti -> td -> Tree a
armar x ti td (NodeT ti td) = NodeT (1 + max(height ti)
										 max( height td)
									x ti td)

--una modificacion para el nodo del arbol y acceder a la altura facilmente seria
--siendo el int la altura que tiene ese nodo
data Tree a = EmptyT 
			  | NodeT Int a (Tree a) (Tree a)

--otra propiedad de AVL es la invariante de que el arbol tiene la informacion de 
--su altura para su nodo

emptyHeap ::  Heap a
addH :: a -> Heap a -> Heap a -> Heap a
deleteH :: Heap a -> Heap a
isEmptyH :: Heap a -> Bool
minH :: Heap a -> a
--lo que hace la clase heap es que tiene una serie de elements ordenados de alguma manera
--y podemos sacarle el minimo de estos
--una heap solo pued entregar el minimo elemento o borrarlo, nada mas
--una heap siempre esta ordenada
{-
en el parcial se evalua:
- invariantes
- que funcione la implementacion
- respetar costos
- respetar abstraccion
-}