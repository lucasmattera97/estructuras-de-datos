data Dir = 
	Izq 
	| Der

data Objeto = 
	Tesoro
	| Chatarra

data Mapa = 
	Cofre Objeto
	| Bifurcacion Objeto Mapa Mapa

mapaGeneral = Bifurcacion Chatarra mapa1 mapa2

mapa1 = Bifurcacion Chatarra mapa3 mapa4

mapa3 = Cofre Chatarra

mapa4 = Cofre Tesoro

mapa2 = Cofre Chatarra

{-
--1. Indica si hay un tesoro en alguna parte del mapa.
hayTesoro :: Mapa -> Bool

--2. Indica si al final del camino hay un tesoro. Nota: el final del camino es la lista vacía de
--direcciones.
hayTesoroEn :: [Dir] -> Mapa -> Bool

--3. Indica el camino al tesoro. Precondición: hay un sólo tesoro en el mapa.
caminoAlTesoro :: Mapa -> [Dir]

--4. Indica el camino de la rama más larga.
caminoRamaMasLarga :: Mapa -> [Dir]

--5. Devuelve los tesoros separados por nivel en el árbol.
tesorosPerLevel :: Mapa -> [[Objecto]]

--6. Devuelve todos lo caminos en el mapa.
todosLosCaminos :: Mapa -> [[Dir]]
-}