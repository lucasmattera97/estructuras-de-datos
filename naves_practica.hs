data Contenedor = Comida | Oxigeno | Torpedo | Combustible
data Componente = Escudo | CanionLaser | Lanzatorpedos | Motor Int | Almacen [Contenedor]
data Nave = Parte Componente Nave Nave | ParteBase

aprovisionados :: [Contenedor] -> Nave -> Bool
aprovisionados [] _ = False
aprovisionados (c:cs) n = (contenedorEnNave c n) && (aprovisionados cs n)

-- devuelve si ese contenedor esta en algun lugar de la nave
contenedorEnNave :: Contenedor -> Nave -> Bool
contenedorEnNave cont (Parte comp n1 n2) = 
	(contenedorEnComponente cont comp) || (contenedorEnNave cont n1) ||(contenedorEnNave cont n2)

contenedorEnComponente :: Contenedor -> Componente -> Bool
contenedorEnComponente c (Almacen cms) = belongContenedor c cms
contenedorEnComponente c _ = False

belongCont :: Contenedor -> [Contenedor] -> Bool
belongContenedor c [] = False
belongContenedor cont (c:cs) = (cont == c) || (belongContenedor cont cs)

	if cont == c
		then True 
		else belongContenedor cont cs

armasNivelN :: Int -> Nave -> [Componente]
armasNivelN _ ParteBase = []
armasNivelN n (Parte c n1 n2) = (comparativa n c) ++ (armasNivelN n1) ++ (armasNivelN n2)

comparativa :: Int -> Componente -> [Componente]
comparativa n Lanzatorpedos = n==0
comparativa n CanionLaser = n==1
comparativa n _ = []







