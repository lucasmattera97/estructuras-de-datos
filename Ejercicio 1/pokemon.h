#include <iostream>

using namespace std;

struct PokemonSt;

typedef PokemonSt* Pokemon;

Pokemon crearP(string nombre, int vida);
string getNombre(Pokemon p);
int getVida(Pokemon p);
void cambiarNombre(Pokemon p, string n);

bool estaVivo(Pokemon p);
void restarVida(Pokemon p);
void lucharN(int n, Pokemon p1, Pokemon p2); ///O(1)
void destruirP(Pokemon& p);
