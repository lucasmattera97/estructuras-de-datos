#include "pokemon.h"


//Ejercicio 1
//Definir el tipo abstracto de datos Pokemon como un n ́umero entero que representa la vida y un
//nombre. Su interfaz es la siguiente:
struct PokemonSt{
    int vida;
    string nombre;
};

Pokemon crearP(string nombreP, int vidaP){
    PokemonSt* pok= new PokemonSt;
    pok->vida=vidaP;
    pok->nombre=nombreP;

    return pok;
}

string getNombre(Pokemon p){
    return p->nombre;
}

int getVida(Pokemon p){
    return p->vida;
}

void cambiarNombre(Pokemon p, string nombre){
    p->nombre = nombre;
}

bool estaVivo(Pokemon p){
    return p->vida>0;
}

//Le resta una unidad a la vida.
void restarVida(Pokemon p){
    p->vida--;
}

void lucharN(int n, Pokemon p, Pokemon r){
    p->vida-=n;
    r->vida-=n;
}

//Libera memoria
void destruirP(Pokemon p){
    delete p;
}

//Implementar esta interfaz destructiva, utilizando memoria din ́amica.

