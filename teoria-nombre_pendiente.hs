--heaps
data Heap a = H (Tree a) [Dir]

addH n (H t ds)= H (addT n t ds) (sig ds)

addT :: a -> Tree a -> [Dir] -> Tree a
addT n t [] = NodeT n EmptyT EmptyT
addT n (NodeT x ti td) (d:ds) = 
	if d == izq
		then intercIzq x (addT n ti ds) td
		else intercDer x ti (addT n td ds) 

intercIzq :: a -> Tree a -> Tree a -> Tree a
intercIzq n (NodeT x ti td) td =
	NodeT (min n x) (NodeT (max n x) tii tid)

intercDer :: a -> Tree a -> Tree a -> Tree a

{-
BUSCAR CODIGO EN LA PAGINA

-}