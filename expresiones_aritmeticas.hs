data Exp = 
	Constante Int
	| ExpUnaria OpUnaria Exp
	| ExpBinaria OpBinaria Exp Exp

data OpUnaria = Neg

data OpBinaria = 
	Suma 
	| Resta 
	| Mult 
	| Div

{-1. Dada una expresión evalúe esta expresión y retorne su valor. 
¿Qué casos hacen que eval sea una función parcial?-}
eval :: Exp -> Int

{-2. Dada una expresión la simplifica según los siguientes criterios:
a) 0 + x = x + 0 = x
b) x − 0 = x
c) 0 − x = −x
d ) x × 1 = 1 × x = x
e) x × 0 = 0 × x = 0-}
simplificar :: Exp -> Exp